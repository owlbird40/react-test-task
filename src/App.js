import React, { Component } from "react";
import { MainLayout } from './layouts'
import { Header, Body } from './partials'

import './App.scss';

class App extends Component {
  constructor (props) {
    super(props)

    this.state = {
      searchStr: null
    }

    this.search = this.search.bind(this);
  }

  search (e) {
    this.setState({ searchStr: e.currentTarget ? e.currentTarget.value : null })
  }
  render() {
    return (
      <MainLayout>
        <Header onChange={e => this.search(e)}></Header>
        <Body searchStr={this.state.searchStr}></Body>
      </MainLayout>
    );
  }
}

export default App;
