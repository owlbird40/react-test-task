import React from 'react'
import './CustomInput.scss'

class CustomInput extends React.Component {
  render () {
    return (
      <div  className="custom-input">
        <input
          value={this.props.value}
          name={this.props.name}
          type={this.props.kind || 'text'}
          placeholder={this.props.placeholder}
          onChange={this.props.onChange}
          maxLength={this.props.max}
        />
        {
          !!this.props.type ?
            (
              <div className={this.props.type}></div>
            ) : null
        }
        {
          !!this.props.max ?
            (
              <div className="max-symbols">Max. {this.props.max} Ch</div>
            ) : null
        }
      </div>
    )
  }
}

export default CustomInput
