import React from 'react'
import './Card.scss'

class Card extends React.Component {
  render () {
    return (
      <div className="card">
        <div className="card-img" style={{ backgroundImage: `url(${this.props.img})` }}></div>
        <div className="card-info">
          <div className="about">
            <div className="category">{this.props.category}</div>
            <div className="weight">{this.props.weight} KCL</div>
          </div>
          <div className="title">
            <div className="yellow"></div>
            {this.props.title}
          </div>
          <div className="description">{this.props.description}</div>
          <div className="total">
            <div className="price">${this.props.price}</div>
            <div className="persons">For {this.props.persons} persons</div>
          </div>
        </div>
      </div>
    )
  }
}

export default Card
