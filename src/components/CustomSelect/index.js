import React from 'react'
import Select from 'react-select'
import './CustomSelect.scss'

class CustomSelect extends React.Component {
  render () {
    const customStyles = {
      control: (base, state) => ({
        ...base,
        height: '48px',
        minHeight: '48px',
        margin: '10px 0',
        paddingRight: '15px',
      }),
      valueContainer: (base) => ({
        ...base,
        padding: '2px 20px',
      }),
      placeholder: (base) => ({
        ...base,
        margin: 0,
      }),
      indicatorSeparator: (base) => ({
        ...base,
        opacity: 0,
      }),
    };
    return (
      <Select
        styles={customStyles}
        options={this.props.options}
        placeholder={<div className="custom-select-placeholder">{this.props.placeholder}</div>}
        theme={theme => ({
          ...theme,
          borderRadius: 5,
          colors: {
            ...theme.colors,
            primary25: '#ffb53a',
            primary: 'rgba(38, 38, 38, 0.1)',
          }
        })}
        onChange={this.props.onChange}
      />
    )
  }
}

export default CustomSelect
