import React from 'react'
import './Button.scss'

class Button extends React.Component {
  render () {
    const { onClick } = this.props;

    return (
      <div className={`button ${!!this.props.inversion ? 'inversion' : null}`} onClick={onClick}>
        <div className="text">{this.props.text}</div>
        <div className={`plus ${!!this.props.inversion ? 'inversion' : null}`}></div>
      </div>
    )
  }
}

export default Button
