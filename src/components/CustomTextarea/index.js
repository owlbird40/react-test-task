import React from 'react'
import './CustomTextarea.scss'

class CustomTextarea extends React.Component {
  render () {
    return (
      <div  className="custom-textarea">
        <textarea
          name={this.props.name}
          value={this.props.value}
          placeholder={this.props.placeholder}
          maxLength="150"
          onChange={this.props.onChange}
        />
        <div className="max-length">Max. 150 Ch</div>
      </div>
    )
  }
}

export default CustomTextarea
