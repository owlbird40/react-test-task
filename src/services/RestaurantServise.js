import React from 'react'

class RestaurantServise {
  static myInstance = null

  dishes = [
    {
      img: '/static/images/840b37dc10f03ea1d204475ef45e9c1c71e3cf13.png',
      category: 'Meat',
      weight: '123',
      title: 'Roasted Butternut Pumpkin, Shiitake Mushroom and Haloumi Salad',
      description: 'A hearty mix of fresh greens, roasted vegetables and golden haloumi makes up this tasty winter salad.',
      price: '66',
      persons: 2,
      ingredients: ['chicken', 'fish']
    },
    {
      img: '/static/images/5f8b3a66cd821d224b75598a3aaac5dc56038abd.png',
      category: 'Meat',
      weight: '243',
      title: 'Slow-cooked, Italian Beef Cheek Ragú with Pappardelle',
      description: 'Slow-cooked beef cheek ragù. Serve with just-cooked pappardelle and sprinkle with lashings of Parmesan, of course.',
      price: '28',
      persons: 2,
      ingredients: ['chicken']
    },
    {
      img: '/static/images/d2c58342bdd2a34932c88b0621297ae2191cb8e2.png',
      category: 'Meat',
      weight: '243',
      title: 'Chicken Cotoletta with Brussels Sprouts, Rocket and Hazelnut Salad',
      description: 'The super-crispy outer also happens to be gluten free.',
      price: '23',
      persons: 2,
      ingredients: ['chicken']
    },
    {
      img: '/static/images/d2c58342bdd2a34932c88b0621297ae2191cb8e2.png',
      category: 'Meat',
      weight: '243',
      title: 'Chicken Cotoletta with Brussels Sprouts, Rocket and Hazelnut Salad',
      description: 'The super-crispy outer also happens to be gluten free.',
      price: '23',
      persons: 2,
      ingredients: ['chicken']
    },
  ]

  static getInstance () {
    if (RestaurantServise.myInstance == null) {
      RestaurantServise.myInstance = new RestaurantServise()
    }
    return this.myInstance
  }

  setDishes (dishes) {
    return this.dishes = dishes
  }

  getAllDishes () {
    return this.dishes
  }

  duplicateDishes () {
    return this.dishes = this.dishes.concat(this.dishes)
  }

  addDish (dish) {
    let result = [dish, ...this.dishes]
    localStorage.setItem('dishes', JSON.stringify(result));
    return this.dishes = result
  }

  filterDishes (searchStr) {
    let res
    if (!searchStr || searchStr.length === 0) {
      res = this.dishes
    } else {
      const str = searchStr.toLowerCase()
      res = this.dishes.filter(dish => {
        return dish.title.toLowerCase().indexOf(str) >= 0 ||
        dish.ingredients.reduce((acc, val) => acc || val.indexOf(str) >= 0, false)
      })
      localStorage.setItem('dishes', JSON.stringify(res));
    }
    return res
  }
}
export default RestaurantServise
