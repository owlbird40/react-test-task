import React from 'react'
import './MainLayout.scss'

class MainLayout extends React.Component {
  render () {
    return (
      <div className="main-layout">
        {this.props.children}
      </div>
    )
  }
}

export default MainLayout
