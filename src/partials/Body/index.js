import React from 'react'
import ReactModal from 'react-modal'
import './Body.scss'
import { Card, Button, CustomInput, CustomSelect, CustomTextarea } from '../../components'
import RestaurantServise from '../../services/RestaurantServise'
ReactModal.setAppElement('#root');

let restaurant = RestaurantServise.getInstance()

class Body extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      showModal: false,
      options: [
        { value: 'meat', label: 'Meat' },
        { value: 'vegetables', label: 'Vegetables' }
      ],
      dishes: null,
      dishName: null,
      dishCategory: null,
      dishDescription: null,
      ingredientsList: [{}],
      totalWeight: null
    }

    this.handleOpenModal = this.handleOpenModal.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
    this.handleScroll = this.handleScroll.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSelect = this.handleSelect.bind(this);
    this.addIngredient = this.addIngredient.bind(this);
    this.handleIngredientChange = this.handleIngredientChange.bind(this);
    this.deleteIngredient = this.deleteIngredient.bind(this);
    this.checkFullFields = this.checkFullFields.bind(this);
    this.checkFullIngredients = this.checkFullIngredients.bind(this);
  }

  handleOpenModal () {
    this.setState({ showModal: true });
  }

  handleCloseModal () {
    this.setState({ showModal: false, totalWeight: 0, dishName: null, dishCategory: null, dishDescription: null, ingredientsList: [{}] });
  }

  handleScroll () {
    const wrappedElement = document.getElementById('dishes');
    if (wrappedElement.getBoundingClientRect().bottom <= window.innerHeight) {
      restaurant.duplicateDishes()
      localStorage.setItem('dishes', JSON.stringify(restaurant.getAllDishes()));
      this.setState({ dishes: restaurant.getAllDishes() })
    }
  }

  handleSelect (value) {
    this.setState({ dishCategory: value })
  }

  handleChange (event) {
    this.setState({
      [event.currentTarget.name]: event.currentTarget.value
    });
  }

  deleteIngredient (index) {
    const arr = [...this.state.ingredientsList]
    arr.splice(index, 1);
    this.setState({ ingredientsList: arr })
    let total = arr.reduce((acc, val) => {
      acc += +val.weight
      return acc
    }, 0)
    this.setState({ totalWeight: total })
  }

  addIngredient () {
    const newItem = this.state.ingredientsList.concat({})
    this.setState({ ingredientsList: newItem })
  }

  handleIngredientChange (e, index, field) {
    const newIngredient = [...this.state.ingredientsList]
    newIngredient[index][field] = e.target.value
    this.setState({ ingredientsList: newIngredient })
    let total = this.state.ingredientsList.reduce((acc, val) => {
      acc += +val.weight
      return acc
    }, 0)
    this.setState({ totalWeight: total })
  }

  checkFullFields () {
    let result = !!this.state.totalWeight &&
      !!this.state.dishName &&
      !!this.state.dishCategory &&
      !!this.state.dishDescription

    return this.state.ingredientsList.reduce((acc, val) => {
      acc = acc && val.name && val.weight
      return acc
    }, result)
  }

  checkFullIngredients () {
    return this.state.ingredientsList.reduce((acc, val) => {
      if (val.name && val.weight) acc += 1
      return acc
    }, 0)
  }

  addDish () {
    const item = {
      img: '/static/images/d2c58342bdd2a34932c88b0621297ae2191cb8e2.png',
      category: this.state.dishCategory.value,
      weight: this.state.totalWeight,
      title: this.state.dishName,
      description: this.state.dishDescription,
      price: '23',
      persons: 2,
      ingredients: this.state.ingredientsList.map(ingredient => ingredient.name)
    }
    restaurant.addDish(item)
    this.setState({ dishes: restaurant.getAllDishes() })
    this.handleCloseModal()
  }

  componentDidMount () {
    window.addEventListener('scroll', this.handleScroll);
  }
  componentWillMount () {
    if (localStorage.getItem('dishes')) {
      restaurant.setDishes(JSON.parse(localStorage.getItem('dishes')))
    }
    localStorage.setItem('dishes', JSON.stringify(restaurant.getAllDishes()));
    this.setState({ dishes: restaurant.getAllDishes() })
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevProps.searchStr !== this.props.searchStr) {
      if (!this.props.searchStr || this.props.searchStr.length === 0) {
        this.setState({ dishes: restaurant.getAllDishes() })
      }
      let filteredDishes = restaurant.filterDishes(this.props.searchStr)
      this.setState({ dishes: filteredDishes })
    }
  }
  componentWillUnmount () {
    window.removeEventListener('scroll', this.handleScroll);
  }

  render () {
    return (
      <div className="body" id="dishes">
        <div className="body-cap">
          <div className="text-wrapper">
            <div className="yellow"></div>
            <div className="text">
              <div className="title">Meat Dishes</div>
              <div className="sub-title">Some of the best meat dishes from worldwide</div>
            </div>
          </div>
          <Button text="Add a new dish" onClick={this.handleOpenModal} />
        </div>
        <ReactModal
          isOpen={this.state.showModal}
          contentLabel="onRequestClose Example"
          onRequestClose={this.handleCloseModal}
          className="modal"
          overlayClassName="overlay"
        >
          <form>
            <div className="modal-header">
              <div className="gradient"></div>
              <div className="title">{this.state.dishName || 'Add a new dish'}</div>
              <div className="sub-title">{this.state.dishDescription || 'Please enter all informations about your new dish'}</div>
              <div className="add-foto">
                <p>Add a photo</p>
                <div className="icon"></div>
              </div>
            </div>
            <div className="modal-body">
              <CustomInput
                name="dishName"
                placeholder="Dish name"
                max="50"
                onChange={this.handleChange}
              />
              <CustomSelect
                value={this.state.dishCategory}
                options={this.state.options}
                placeholder="Select a dish category"
                onChange={this.handleSelect}
              />
              <CustomTextarea
                name="dishDescription"
                placeholder="Dish description"
                onChange={this.handleChange}
              />
              <div className="ingredients">
                <div className="title">Ingredients</div>
                <Button text="Add a new ingredient" inversion onClick={this.addIngredient} />
              </div>
              <div className="ingredients-list">
                <div className="icon"></div>
                <div className="info-wrapper">
                  {this.state.ingredientsList.map((ingredient, index) => (
                    <div className="info" key={index}>
                      <CustomInput
                        value={this.state.ingredientsList[index].name || ''}
                        name="ingredientName"
                        type="plus"
                        placeholder="Ingredient name"
                        onChange={e => this.handleIngredientChange(e, index, 'name')}
                      />
                      <CustomInput
                        value={this.state.ingredientsList[index].weight || ''}
                        name="ingredientWeight"
                        kind="number"
                        placeholder="Weight (Kcl)"
                        onChange={e => this.handleIngredientChange(e, index, 'weight')}
                      />
                      {ingredient.weight || ingredient.name ?
                        ( <div className="trash" onClick={() => this.deleteIngredient(index)}></div> )
                         : null
                       }
                    </div>
                  ))}
                </div>
              </div>
              {
                this.checkFullFields() ? (
                  <React.Fragment>
                    <div className="total-wrapper">
                      <div className="total">
                        <div className="info"><b>{this.checkFullIngredients() ? this.state.ingredientsList.length : 0} Ingredients</b> in your dish</div>
                        <div className="weight">Total weight : <b>{this.state.totalWeight || 0} Kcl</b></div>
                      </div>
                    </div>
                    <div className="add" onClick={() => this.addDish()}>Add this dish to my menu <p>{this.state.totalWeight || 0} Kcl</p><div className="plus"></div></div>
                  </React.Fragment>
                ) : null
              }
            </div>
          </form>
        </ReactModal>
        {this.state.dishes.map((dish, index) => (
          <Card
            key={index}
            img={dish.img}
            category={dish.category.toUpperCase()}
            weight={dish.weight}
            title={dish.title}
            description={dish.description}
            price={dish.price}
            persons={dish.persons}
          />
        ))}
      </div>
    )
  }
}

export default Body
