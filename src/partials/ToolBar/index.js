import React from 'react'
import './ToolBar.scss'
import { CustomInput } from '../../components'

class ToolBar extends React.Component {
  render () {
    return (
      <div className="toolbar">
        <div className="logo"></div>
        <div className="menu">
          <div className="menu-item">Our Restaurant</div>
          <div className="menu-item">Menu</div>
          <div className="menu-item">Contact us</div>
        </div>
        <CustomInput type="search" placeholder="Try « Chicken cotoletta »" onChange={this.props.onChange} />
        <div className="account">
          <div className="name">John C.</div>
          <div className="icon"></div>
        </div>
      </div>
    )
  }
}

export default ToolBar
