import React from 'react'
import './Header.scss'
import { ToolBar } from '../../partials'
import RestaurantServise from '../../services/RestaurantServise'

let restaurant = RestaurantServise.getInstance()

class Header extends React.Component {
  render () {
    return (
      <div className="header">
        <ToolBar onChange={e => this.props.onChange(e)}></ToolBar>
        <div className="header-title">Menu</div>
      </div>
    )
  }
}

export default Header
